<nav class="uk-navbar-container" uk-navbar>
    <div class="uk-navbar-left">
        <ul class="uk-navbar-nav">
            <li><a href="{{route('home')}}">Home</a></li>
            <li><a href="{{route('razas')}}">Razas</a></li>
            <li><a href="{{route('sexo')}}">Sexo</a></li>
            <li><a href="{{route('tamano')}}">Tamano</a></li>
        </ul>
    </div>
</nav>
